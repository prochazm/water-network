import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.TextAttribute;
import java.awt.geom.*;
import java.text.AttributedString;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * MainPanel
 *
 * Child of JPanel, basically just takes care
 * that whole WaterNetwork is displayed correctly
 */
public class MainPanel extends JPanel {
    /** Stroke for outlining pipes */
    private static final BasicStroke PIPE_OUTLINE_STROKE = new BasicStroke(17F, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10);
    /** Stroke for drawing the insides of pipes */
    private static final BasicStroke PIPE_INSIDE_STROKE = new BasicStroke(16F, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10);
    /** Default stroke for the rest */
    private static final BasicStroke DEFAULT_STROKE = new BasicStroke();
    /** Special blue for water */
    private static final Color WATER_BLUE = new Color(96, 195, 255);

    /** Allows rendering of frame time counter */
    private final boolean PERF_MON = true;
    /** Everything smaller than that will be considered as there is no water flowing */
    private final float FLOW_RESOLUTION = 0.01F;

    /** Size of reservoir */
    private GlyphSize glyphSize;

    /** Space between panel border and left/right most node */
    private double xOffset = 0;
    /** Space between panel border and top/bottom most node */
    private double yOffset = 0;
    /** Transformation coefficient of x axis from model to panel */
    private double xRecount = 1;
    /** Transformation coefficient of y axis from model to panel */
    private double yRecount = 1;

    /** Reference to visualized network */
    private WaterNetwork waterNetwork;
    private Map<Reservoir, ReservoirDataSet> reservoirDataSetMap = new HashMap<>();
    private Map<Pipe, PipeDataSet> pipeDataSetMap = new HashMap<>();

    /** total ms spent redrawing panel (with {@code PERF_MON = true}) */
    private long msSpentInPaint;
    /** count of redraws (with {@code PERF_MON = true}) */
    private long redrawCounter;

    /**
     * initializes waterNetwork and glyphSize with passed
     * arguments
     * @see WaterNetwork
     * @param waterNetwork network that will be visualized
     * @param glyphSize restricts size of some elements
     */
    public MainPanel(final WaterNetwork waterNetwork, final GlyphSize glyphSize){
        this.waterNetwork = waterNetwork;
        this.glyphSize = glyphSize;

        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {

            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                for (ReservoirDataSet r : reservoirDataSetMap.values()) {
                    if (r.triggers(mouseEvent.getPoint())) {
                        JFrame frame = new JFrame();
                        frame.setLocationRelativeTo(null);

                        GraphPanel panel = new GraphPanel();
                        frame.add(panel);
                        panel.setPreferredSize(new Dimension(640, 480));

                        panel.setGraph(r.getGraph());
                        frame.pack();
                        frame.setVisible(true);
                        return;
                    }
                }

                for (PipeDataSet p : pipeDataSetMap.values()) {
                    if (p.triggers(mouseEvent.getPoint())) {
                        JFrame frame = new JFrame();
                        frame.setLocationRelativeTo(null);

                        GraphPanel panel = new GraphPanel();
                        frame.add(panel);
                        panel.setPreferredSize(new Dimension(640, 480));

                        panel.setGraph(p.getGraph());
                        frame.pack();
                        frame.setVisible(true);
                        return;
                    }
                }
            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {

            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {

            }
        });
    }

    /**
     * Redraws whole panel, if <code>PERF_MON</code> is set
     * true then it also renders frame time in the bottom
     * right corner of panel.
     * @param g graphics context
     */
    public void paintComponent(Graphics g){
        long startTime = System.currentTimeMillis();
        // cast g to Graphics2D for some fancy graphics
        Graphics2D g2d = (Graphics2D) g;
        // clear the panel before drawing anything
        g2d.clearRect(0,0,this.getWidth(), this.getHeight());
        // use antialiasing
        g2d.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON
        );

        // recalculate offsets and recounts
        initScale(this.getWidth(), this.getHeight(), this.glyphSize.get(), this.waterNetwork);
        // draw the network
        drawNetwork(this.waterNetwork, g2d);

        // this will render frame times if true
        if (PERF_MON) {
            long frameTime = System.currentTimeMillis() - startTime;
            this.msSpentInPaint += frameTime;
            g2d.drawString(
                    String.format("Frame time: %d [ms]", frameTime),
                    this.getWidth() - 130,
                    this.getHeight() - 20
            );
            g2d.drawString(
                    String.format("AVG: %.2f", (float) this.msSpentInPaint / ++this.redrawCounter),
                    this.getWidth() - 130,
                    this.getHeight() - 6
            );
        }
    }

    /**
     * Draws reservoir as rectangle of size at position passed
     * as argument with part of it colored blue symbolizing how
     * filled it is.
     * @see Reservoir
     * @param position position of reservoir
     * @param size size of reservoir
     * @param filled real number on interval <0;1>
     *               meaning how filled the reservoir is
     * @param g2d graphics context
     */
    private Area drawReservoir(final Point2D position, final int size,
                               final double filled, final Graphics2D g2d){

        // create rectangle representing the reservoir
        Rectangle2D.Double reservoir = new Rectangle2D.Double(
                position.getX(),
                position.getY(),
                size, size
        );

        // draw fill it gray
        g2d.setColor(Color.LIGHT_GRAY);
        g2d.fill(reservoir);
        g2d.setColor(WATER_BLUE);
        // create new rectangle representing water in reservoir and fill it blue
        g2d.fill(new Rectangle2D.Double(
                position.getX(),
                position.getY() + (1 - filled) * size,
                size,
                filled * size)
        );
        // draw border around the reservoir
        g2d.setColor(Color.BLACK);
        g2d.draw(reservoir);

        return new Area(reservoir);
    }

    /**
     * Draws node as filled circle with outline. Use this
     * only for non-reservoir nodes. There is drawReservoir
     * for instances of Reservoir.
     * @see NetworkNode
     * @param position where the node is drawn
     * @param size size of node in pixels
     * @param g2d graphics context
     */
    private void drawNode(final Point2D position, final double size, final Graphics2D g2d){
        // create ellipse around position
        Ellipse2D node = new Ellipse2D.Double(
                position.getX() + (this.glyphSize.get() - size) * 0.5,
                position.getY() + (this.glyphSize.get() - size) * 0.5,
                size, size);

        // and draw it
        g2d.setColor(Color.GRAY);
        g2d.fill(node);
        g2d.setColor(Color.BLACK);
        g2d.draw(node);
    }

    /**
     * Draw valve around given point. Openness of the valve
     * depends on open argument which is basically percentage
     * divided by 100. The opened part of valve is colored
     * blue or gray depending on flow. Size of the valve is
     * equal to size argument and it is in pixels.
     * Id is drawn inside the valve.
     * @param point point where the valve is drawn
     * @param open how much is the valve opened
     * @param flow how much water flows through the valve
     * @param size diameter of valve in pixels
     * @param id pipe's id
     * @param g2d graphics context
     */
    private void drawValve(final Point2D point, final double open, final double flow,
                           final int size, int id, final Graphics2D g2d){

        final double halfSize = size * 0.5;

        // base for the valve
        Ellipse2D ellipse = new Ellipse2D.Double(
                point.getX() - halfSize,
                point.getY() - halfSize,
                size, size
        );

        // convert the base to area
        Area fill = new Area(ellipse);
        Area overlap = new Area(
                new Rectangle2D.Double(
                        point.getX() - halfSize,
                        point.getY() - halfSize,
                        size,
                        size * open
                ));
        fill.subtract(overlap);

        // draw the valve with fancy colors
        g2d.setColor((Math.abs(flow) > 0.01) ? WATER_BLUE : Color.LIGHT_GRAY);
        g2d.fill(ellipse);
        g2d.setColor(Color.DARK_GRAY);
        g2d.fill(fill);
        g2d.setColor(Color.BLACK);
        g2d.draw(ellipse);

        FontMetrics metrics = g2d.getFontMetrics();
        String s = Integer.toString(id);
        g2d.setColor(Color.WHITE);
        g2d.drawString(s, (int)(point.getX() - metrics.stringWidth(s) * 0.5), (int)(point.getY() + metrics.getAscent() * 0.5));
        g2d.setColor(Color.BLACK);
        g2d.drawString(s, (int)(point.getX() - metrics.stringWidth(s) * 0.5) - 1, (int)(point.getY() + metrics.getAscent() * 0.5) - 1);
    }

    /**
     * Draws pipe between two nodes. It's drawn blue of gray depending on
     * the water flow. If the water flow is too small meaning less than
     * 10^-2, then it's considered nothing flows there. After pipes are
     * drawn call drawFlowStatus.
     * @see Pipe
     * @param start first Node
     * @param end second Node
     * @param size size of valves
     * @param flow flow of water in m^3s^-1, sign determines direction
     * @param open real number in <0;1> interval determining how much opened the valve is
     * @param id pipe's id
     * @param g2d graphics context
     * @return line
     */
    private Line2D drawPipe(final Point2D start, final Point2D end, final int size,
                          final double flow, final double open, final int id, final Graphics2D g2d){
        // adjust start and end coordinates
        final double startXinPx = start.getX() + this.glyphSize.get() * 0.5;
        final double startYinPx = start.getY() + this.glyphSize.get() * 0.5;
        final double endXinPx = end.getX() + this.glyphSize.get() * 0.5;
        final double endYinPx = end.getY() + this.glyphSize.get() * 0.5;

        // draw the pipe black with thick brush
        Line2D line = new Line2D.Double(startXinPx, startYinPx, endXinPx, endYinPx);
        g2d.setColor(Color.BLACK);
        g2d.setStroke(PIPE_OUTLINE_STROKE);
        g2d.draw(line);

        // now draw it again with thinner blue or gray brush
        g2d.setColor((Math.abs(flow) > 0.01) ? WATER_BLUE : Color.LIGHT_GRAY);
        g2d.setStroke(PIPE_INSIDE_STROKE);
        g2d.draw(line);

        // and then let valves and arrows be drawn
        g2d.setStroke(DEFAULT_STROKE);
        this.drawFlowStatus(startXinPx, startYinPx, endXinPx, endYinPx, flow, size, open, id, g2d);


        return line;
    }

    /**
     * Calls drawValve to draw valve somewhere near quarter of the pipe.
     * Then draw an arrow in the middle of the pipe. Arrow has the
     * same size no matter how much you resize the panel it also points
     * direction of the water flow. Finally string is drawn somewhere
     * near the arrow with information about how much water is flowing
     * through the pipe. Resolution of water flow is 10^-2 m^3s^-1, if
     * there is less water flowing it's considered like it's zero and
     * arrows with string aren't drawn at all.
     * @param startXinPx x coordinate of first node
     * @param startYinPx y coordinate of first node
     * @param endXinPx x coordinate of second node
     * @param endYinPx y coordinate of second node
     * @param flow flow of water in m^3s^-1, sign determines direction
     * @param size valve size
     * @param open real number in <0;1> interval determining how much opened the valve is
     * @param id pipe's id
     * @param g2d graphics context
     */
    private void drawFlowStatus(final double startXinPx, final double startYinPx,
                            final double endXinPx, final double endYinPx,
                            final double flow, final int size, final double open,
                            final int id, final Graphics2D g2d){

        // draw the valve first
        final double pipeLength = Math.sqrt(Math.pow(startXinPx - endXinPx, 2) + Math.pow(startYinPx - endYinPx, 2));
        final Point2D valveLocation = new Point2D.Double(
                startXinPx * ((0.2 + size / pipeLength)) + endXinPx * (0.8 - size / pipeLength),
                startYinPx * ((0.2 + size / pipeLength)) + endYinPx * (0.8 - size / pipeLength)
        );
        drawValve(valveLocation, open, flow, size, id, g2d);

        // if water flow is too low, don't bother
        if (Math.abs(flow) < this.FLOW_RESOLUTION){
            return;
        }

        // calculate coordinates of the base line for arrow
        final double lowerMultiplier = (0.50 - 10 / pipeLength);
        final double upperMultiplier = (0.50 + 10 / pipeLength);

        final double arrowX = startXinPx * upperMultiplier + endXinPx * lowerMultiplier;
        final double arrowY = startYinPx * upperMultiplier + endYinPx * lowerMultiplier;
        final double arrowX2 = startXinPx * lowerMultiplier + endXinPx * upperMultiplier;
        final double arrowY2 = startYinPx * lowerMultiplier + endYinPx * upperMultiplier;

        // now draw it
        g2d.draw(new Line2D.Double(arrowX, arrowY, arrowX2, arrowY2));

        // calculate points at side of the line
        double vectX = arrowX2 - arrowX;
        double vectY = arrowY2 - arrowY;
        final double arrowLength = Math.sqrt(vectX*vectX + vectY*vectY);
        vectX /= arrowLength;
        vectY /= arrowLength;
        double lx, ly, rx, ry;

        // they are placed differently depending on the flow
        if (flow > 0){
            lx = arrowX2 - 10*vectX + 5*vectY;
            ly = arrowY2 - 10*vectY - 5*vectX;
            rx = arrowX2 - 10*vectX - 5*vectY;
            ry = arrowY2 - 10*vectY + 5*vectX;
            g2d.draw(new Line2D.Double(arrowX2, arrowY2, lx, ly));
            g2d.draw(new Line2D.Double(arrowX2, arrowY2, rx, ry));
        } else {
            lx = arrowX + 10*vectX + 5*vectY;
            ly = arrowY + 10*vectY - 5*vectX;
            rx = arrowX + 10*vectX - 5*vectY;
            ry = arrowY + 10*vectY + 5*vectX;
            g2d.draw(new Line2D.Double(arrowX, arrowY, lx, ly));
            g2d.draw(new Line2D.Double(arrowX, arrowY, rx, ry));
        }

        // create strings needed we need to display and format them
        String flowAsString = String.format("%.2f ", Math.abs(flow));
        double flowWidth = g2d.getFontMetrics().stringWidth(flowAsString);
        AttributedString string = new AttributedString("m3s-1");
        string.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUPER, 1, 2);
        string.addAttribute(TextAttribute.SUPERSCRIPT, TextAttribute.SUPERSCRIPT_SUPER, 3, 5);

        // draw those strings at some silly position so it doesn't collide with other objects.
        g2d.drawString(flowAsString, (int)(arrowX + 20), (int)(arrowY + 20));
        g2d.drawString(string.getIterator(), (int)(arrowX + 20 + flowWidth), (int)(arrowY + 20));
    }

    /**
     * Calculates offset from panel border and coefficient of
     * x and y axis transformation against the WaterNetwork
     * coordinate system.
     * @param panelWidth width of panel
     * @param panelHeight height of panel
     * @param size size of reservoirs
     * @param waterNetwork WaterNetwork instance to initialize variables with
     */
    private void initScale(final int panelWidth, final int panelHeight,
                           final int size, final WaterNetwork waterNetwork){
        // find highest y of all nodes
        double maxY = Math.max(Stream.of(waterNetwork.getAllNetworkNodes())
                .mapToDouble(n -> n.position.getY())
                .max()
                .getAsDouble(), 1);
        // find highest x of all nodes
        double maxX = Math.max(Stream.of(waterNetwork.getAllNetworkNodes())
                .mapToDouble(n -> n.position.getX())
                .max()
                .getAsDouble(), 1);

        // calculate offset as lower of 5% of panelWidth or Height and 20px
        this.xOffset = Math.min(panelWidth * 0.05, 20);
        this.yOffset = Math.min(panelHeight * 0.05, 20);
        // calculate transformation from model to panel coordinate system
        this.xRecount = (panelWidth - 2 * xOffset - size) / maxX;
        this.yRecount = (panelHeight - 2 * yOffset - size) / maxY;
    }

    /**
     * Returns instance of Point2D passed as argument transformed to
     * coordinate system of this panel.
     * @param model point to be transformed
     * @return transformed model
     */
    private Point2D modelToWindow(final Point2D model){
        return new Point2D.Double(model.getX() * this.xRecount + this.xOffset,
                model.getY() * this.yRecount + this.yOffset);
    }

    /**
     * Iterates through pipes and nodes in WaterNetwork instance passed
     * as parameter and call respective methods to draw them onto this
     * panel. Datasets are also initialized for each measured network object.
     * @see WaterNetwork
     * @param waterNetwork instance of network to be drawn
     * @param g2d graphics context
     */
    private void drawNetwork(final WaterNetwork waterNetwork, final Graphics2D g2d){
        // draw pipes
        int counter = 0;
        for (Pipe pipe : waterNetwork.getAllPipes()){
            Line2D line = drawPipe(modelToWindow(pipe.start.position), modelToWindow(pipe.end.position), 25, pipe.flow, pipe.open, ++counter, g2d);
            PipeDataSet pipeDataSet;
            if (this.pipeDataSetMap.containsKey(pipe)){
                pipeDataSet = this.pipeDataSetMap.get(pipe);
            } else {
                pipeDataSet = new PipeDataSet(waterNetwork, pipe);
                pipeDataSet.autoCollect(50);
                this.pipeDataSetMap.put(pipe, pipeDataSet);
            }
            pipeDataSet.setTriggerShape(line);
        }

        // draw nodes
        for (NetworkNode node : waterNetwork.getAllNetworkNodes()) {
            if (node instanceof Reservoir){
                Area a = drawReservoir(
                        modelToWindow(node.position),
                        this.glyphSize.get(),
                        ((Reservoir) node).content/((Reservoir) node).capacity,
                        g2d);
                ReservoirDataSet reservoirDataSet;
                if (this.reservoirDataSetMap.containsKey(node)){
                    reservoirDataSet = this.reservoirDataSetMap.get(node);
                } else {
                    reservoirDataSet = new ReservoirDataSet((Reservoir)node, waterNetwork);
                    reservoirDataSet.autoCollect(50);
                    this.reservoirDataSetMap.put((Reservoir)node, reservoirDataSet);
                }
                reservoirDataSet.setTriggerShape(a);
                continue;
            }
            drawNode(modelToWindow(node.position), Math.max(this.glyphSize.get() * 0.25, 23), g2d);
        }
    }
}

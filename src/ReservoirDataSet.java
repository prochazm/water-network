import java.awt.*;
import java.awt.geom.Line2D;
import java.util.TreeMap;

/**
 * ReservoirDataSet
 *
 * Implementation of ADataSet for Reservoirs
 */
public class ReservoirDataSet extends ADataSet{
    private Reservoir reservoir;

    /**
     * Initializes DataSet for passed {@code Reservoir} inside
     * passed {@code WaterNetwork}
     * @param reservoir measured Reservoir
     * @param waterNetwork WaterNetwork containing the Reservoir
     */
    public ReservoirDataSet(Reservoir reservoir, WaterNetwork waterNetwork){
        super(waterNetwork);
        this.reservoir = reservoir;
    }

    /**
     * Calls {@code putAndTrim} with reservoirs current water level.
     */
    @Override
    public void collectData(){
        super.putAndTrim(this.reservoir.content);
    }

    /**
     * Create XYGraph of this DataSet and return it.
     * @return XYGraph
     */
    @Override
    XYGraph getGraph() {
        XYGraph g = new XYGraph((TreeMap<Double, Double>) this.dataSet.clone());
        g.setLegend("time [m]", "volume [m^3]");
        return g;
    }

    /**
     * Returns true if Point lies inside Reservoir's
     * gui representation.
     * @param point Point to be tested
     * @return
     */
    @Override
    boolean triggers(Point point) {
        return super.triggerShape.contains(point);
    }
}

import java.awt.*;
import java.awt.geom.*;
import java.util.*;

public class XYGraph {
    TreeMap<Double, Double> dataSet;
    double xMin, xMax, xSpan, width;
    double yMin, yMax, ySpan, height;

    double[] ticksX;
    double[] ticksY;

    String xLegend = "";
    String yLegend = "";

    public XYGraph(TreeMap<Double, Double> dataSet){
        this.dataSet = dataSet;

        xMin = 0;
        xMax = dataSet.lastKey();
        xSpan = xMax - xMin;

        yMin = Double.min(dataSet.values().stream().min(Comparator.comparingDouble(Double::doubleValue)).get(), 0);
        yMax = dataSet.values().stream().max(Comparator.comparingDouble(Double::doubleValue)).get();
        ySpan = yMax - yMin;

        computeTicks(5);
    }

    public void setLegend(String xLegend, String yLegend){
        this.xLegend = xLegend;
        this.yLegend = yLegend;
    }

    private void computeTicks(int n) {
        if (yMin < 0){
            ++n;
        }
        ticksX = new double[n];
        ticksY = new double[n];
        ticksY[--n] = 0;
        double stepX = xSpan / (n + 1);
        double stepY = ySpan / (n + 1);

        for (int i = 0; i < n; ++i){
            ticksX[i] = xMin + (1+i) * stepX;
            ticksY[i] = yMin + (1+i) * stepY;
        }
    }

    public void draw(Graphics2D g2d, double width, double height){
        this.width = width;
        this.height = height;

        g2d.draw(new Rectangle2D.Double(0, 0, width, height));

        drawData(g2d);
        drawGrid(g2d);
        drawAxes(g2d);
        drawTicks(g2d);
    }

    private void drawData(Graphics2D g2d) {
        Path2D.Double path = new Path2D.Double();
        path.moveTo(data2windowX(dataSet.firstEntry().getKey()), data2windowY(dataSet.firstEntry().getValue()));

        for (Map.Entry<Double, Double> entry : dataSet.entrySet()) {
            double wx = data2windowX(entry.getKey());
            double wy = data2windowY(entry.getValue());
            path.lineTo(wx, wy);
        }

        Stroke s = g2d.getStroke();
        g2d.setColor(Color.GREEN);
        g2d.setStroke(new BasicStroke(2F, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND, 10));
        g2d.draw(path);
        g2d.setStroke(s);
        g2d.setColor(Color.BLACK);

    }

    private double data2windowY(double y) {
        return (1 - ((y - yMin)/ySpan)) * height;
    }

    private double data2windowX(double x) {
        return (x - xMin)/xSpan * width;
    }

    private void drawTicks(Graphics2D g2d) {
        FontMetrics m = g2d.getFontMetrics(g2d.getFont());

        for(int i = 0; i < ticksX.length; ++i){
            String text = String.format("%.1f", ticksX[i]);
            double x = data2windowX(ticksX[i]) - m.stringWidth(text) / 2;
            double y = data2windowY(yMin) + m.getAscent() * 2;
            g2d.drawString(text, (float)x, (float)y);
        }

        for (int i = 0; i < ticksY.length; ++i){
            String text = String.format("%.3g", ticksY[i]);
            double x = data2windowX(xMin) - m.stringWidth(text) - 5;
            double y = data2windowY(ticksY[i]);
            g2d.drawString(text, (float)x, (float) y);
        }

    }

    private void drawAxes(Graphics2D g2d) {
        FontMetrics m = g2d.getFontMetrics(g2d.getFont());
        AffineTransform t = g2d.getTransform();
        g2d.rotate(Math.PI * -0.5);
        g2d.drawString(yLegend, (int)((-this.height * 0.5) - (m.stringWidth(yLegend)*0.5)),
                -4* m.getAscent());
        g2d.setTransform(t);
        g2d.drawString(xLegend, (int)(this.width * 0.5 - m.stringWidth(xLegend)*0.5), (int)(this.height + 4* m.getAscent()));
    }

    private void drawGrid(Graphics2D g2d) {
        g2d.setColor(Color.GRAY);
        for (int i = 0; i < ticksY.length; ++i) {
            double y = data2windowY(ticksY[i]);
            double x1 = data2windowX(xMin);
            double x2 = data2windowX(xMin + xSpan);
            if (ticksY[i] == 0){
                g2d.setColor(Color.RED);
                g2d.draw(new Line2D.Double(x1, y, x2, y));
                g2d.setColor(Color.BLACK);
                continue;
            }
            g2d.draw(new Line2D.Double(x1, y, x2, y));
        }
        for (int i = 0; i < ticksX.length; ++i){
            double x = data2windowX(ticksX[i]);
            double y1 = data2windowY(yMin);
            double y2 = data2windowY(yMin + ySpan);
            g2d.draw(new Line2D.Double(x, y1, x, y2));
        }
        g2d.setColor(Color.BLACK);
    }
}

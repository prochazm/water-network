import java.awt.*;
import java.awt.geom.Line2D;
import java.util.TreeMap;

/**
 * PipeDataSet
 *
 * Implementation of ADataSet for Pipes.
 */
public class PipeDataSet extends ADataSet {
    private Pipe pipe;

    /**
     * Initialize PipeDataSet for passed {@code Pipe} inside
     * passed {@code WaterNetwork}
     * @param waterNetwork
     * @param pipe
     */
    public PipeDataSet(WaterNetwork waterNetwork, Pipe pipe) {
        super(waterNetwork);
        this.pipe = pipe;
    }

    /**
     * Calls {@code putAndTrim} with pipe's current flow.
     */
    @Override
    public void collectData(){
        super.putAndTrim(this.pipe.flow);
    }

    /**
     * Create XYGraph of this DataSet and return it.
     * @return XYGraph
     */
    @Override
    XYGraph getGraph() {
        XYGraph g = new XYGraph((TreeMap<Double, Double>) this.dataSet.clone());
        g.setLegend("time [m]", "flow speed [m^3 * s^-1]");
        return g;
    }

    /**
     * Returns true if Point lies inside Pipe's
     * gui representation.
     * @param point Point to be tested
     * @return
     */
    @Override
    boolean triggers(Point point) {
        final double PIPE_RADIUS = 10;
        Line2D l = (Line2D)super.triggerShape;

        if (point.x - PIPE_RADIUS > Double.max(l.getX1(),l.getX2()) || point.x + PIPE_RADIUS < Double.min(l.getX1(), l.getX2())){
            return false;
        } else if (point.y - PIPE_RADIUS > Double.max(l.getY1(),l.getY2()) || point.y + PIPE_RADIUS < Double.min(l.getY1(), l.getY2())){
            return false;
        }

        double dist = Math.abs((l.getY2() - l.getY1())*point.x - (l.getX2() - l.getX1())*point.y + l.getX2()*l.getY1() - l.getY2()*l.getX1()) /
                Math.sqrt(Math.pow(l.getY2()-l.getY1(), 2) + Math.pow(l.getX2() - l.getX1(), 2));

        return dist < PIPE_RADIUS;
    }
}

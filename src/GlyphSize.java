/**
 * GlyphSize
 *
 * Represents size of GlyphSize. It's a simple
 * implementation of mutable integer for easy
 * glyph size change propagation.
 */
public class GlyphSize {
    int value;

    /**
     * Initialize with initial value.
     * @param value initial value
     */
    public GlyphSize(int value){
        set(value);
    }

    /**
     * getter
     * @return glyph size
     */
    public int get(){
        return value;
    }

    /**
     * setter
     * @param value desired glyph size
     */
    public void set(int value){
        this.value = value;
    }
}

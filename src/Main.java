import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;

/**
 * Main
 *
 * This is the MAIN class
 */
public class Main {
    /** Implicit value of glyphSize */
    private static final int DEFAULT_GLYPH_SIZE = 40;
    private static WaterNetwork waterNetwork;
    private static GlyphSize glyphSize;

    /**
     * Prepares window, initialize new WaterNetwork
     * and set up timer for refreshing the graphics
     * and updating animation.
     * @param args arguments from terminal
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        waterNetwork = new WaterNetwork(4);
        glyphSize = new GlyphSize(args.length > 0 ? Integer.parseInt(args[0]) : DEFAULT_GLYPH_SIZE);
        MainPanel mainPanel = new MainPanel(waterNetwork, glyphSize);
        mainPanel.setPreferredSize(new Dimension(400, 400));
        mainPanel.setDoubleBuffered(true);

        composeControlls(frame, mainPanel);

        //frame.add(mainPanel);
        frame.setLocationRelativeTo(null);
        frame.setMinimumSize(new Dimension(100, 100));
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Water network");

        waterNetwork.runNormal();
        Timer timer = new Timer(100, e -> {
            waterNetwork.updateState();
            mainPanel.repaint();
        });
        timer.start();
    }

    /**
     * Put controls in place
     * @param frame parrent frame
     * @param mainPanel simulation panel
     */
    public static void composeControlls(JFrame frame, MainPanel mainPanel){
        JPanel panel = new JPanel();
        frame.add(panel);
        panel.setLayout(new BorderLayout());
        panel.add(mainPanel, BorderLayout.CENTER);

        JPanel controlsPanel = new JPanel();
        controlsPanel.setLayout(new BoxLayout(controlsPanel, BoxLayout.PAGE_AXIS));
        panel.add(controlsPanel, BorderLayout.EAST);
        controlsPanel.setMaximumSize(new Dimension(Integer.MAX_VALUE, 200));

        JCheckBox simSpeedChkb = new JCheckBox("Run fast");
        JSlider glyphSizeSlider = new JSlider();
        glyphSizeSlider.setMinimum(15);
        glyphSizeSlider.setMaximum(100);
        glyphSizeSlider.setValue(glyphSize.get());

        controlsPanel.add(simSpeedChkb);
        controlsPanel.add(new JLabel("Glyph size: "));
        controlsPanel.add(glyphSizeSlider);

        simSpeedChkb.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                waterNetwork.runFast();
            } else if (e.getStateChange() == ItemEvent.DESELECTED){
                waterNetwork.runNormal();
            }
        });

        glyphSizeSlider.addChangeListener(e -> {
            if (glyphSizeSlider.getValueIsAdjusting()) {
                glyphSize.set(glyphSizeSlider.getValue());
                System.out.println(glyphSize.get());
            }
        });

        int counter = 0;
        for (Pipe pipe : waterNetwork.getAllPipes()) {
            JLabel label = new JLabel(String.format("Valve %d:",  ++counter));
            JSlider valveSlider = new JSlider();
            valveSlider.setMinimum(0);
            valveSlider.setMaximum(100);
            valveSlider.setValue((int)(pipe.open * 100));
            valveSlider.addChangeListener(e -> {
                if (valveSlider.getValueIsAdjusting()) {
                    pipe.open = valveSlider.getValue() / 100.;
                }
            });
            controlsPanel.add(label);
            controlsPanel.add(valveSlider);
        }

    }
}

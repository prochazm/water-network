import javax.swing.*;
import java.awt.*;

/**
 * GraphPanel
 *
 * Extension to JPanel for displaying XYGraphs
 */
public class GraphPanel extends JPanel{
    XYGraph graph;

    /**
     * Paint the graph
     * @param g context
     */
    public void paintComponent(Graphics g){
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON
        );

        if (graph != null){
            g2d.translate(80, 50);
            graph.draw(g2d, this.getWidth() - 100, this.getHeight() - 130);
        }
    }

    /**
     * Set graph that will be displayed
     * @param graph graph
     */
    public void setGraph(XYGraph graph){
        this.graph = graph;
    }
}

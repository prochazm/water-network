import javax.swing.*;
import java.awt.*;
import java.awt.geom.Area;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * ADataSet
 *
 * Abstract class meant to either manually or automatically (using a timer)
 * collect data. These data can then be returned as instance of XYGraph which
 * can then be plotted.
 */
public abstract class ADataSet {
    private WaterNetwork waterNetwork;
    Shape triggerShape;
    TreeMap<Double, Double> dataSet;
    Timer timer;

    private int desiredMaxDataLength = 100;
    private final double MAX_DATA_LENGTH_RESIZE_EFFORT = 0.9;
    private final double MAX_DATA_LENGTH_RESIZE_MULTIPLIER = 1.1;

    final double MIN_ALLOWED_DIFFERENCE = 0.001;

    /**
     * Initialize ADataSet
     * WaterNetwork reference is stored for getting accurate
     * time readings.
     * @param waterNetwork
     */
    public ADataSet(WaterNetwork waterNetwork){
        this.waterNetwork = waterNetwork;
        this.dataSet = new TreeMap<>();
    }

    /**
     * Very simple way to reduce data set size. In this simulation
     * everything tends to stabilize at constant value (flow, reservoir
     * water level). So what I'm doing here is basically throwing away
     * chunks of data when the first derivative between them is very close
     * to zero (defined by {@code MIN_ALLOWED_DIFFERENCE}).
     */
    private void reduceDataSet(){
        Map.Entry<Double, Double> previous = null;
        Map.Entry<Double, Double> start = null;
        boolean buildingChunk;
        ArrayList<Double> removedChunk = new ArrayList<>();

        for (Map.Entry<Double, Double> entry : dataSet.entrySet()) {
            if (previous == null){
                previous = entry;
                start = entry;
                continue;
            }
            double derivative = (entry.getValue() - start.getValue())
                    / (entry.getKey() - start.getKey());

            if (Math.abs(derivative) < MIN_ALLOWED_DIFFERENCE){
                buildingChunk = true;
                removedChunk.add(previous.getKey());
            } else {
                buildingChunk = false;
            }
            previous = entry;
            if (!buildingChunk){
                start = previous;
            }
        }

        for (Double d : removedChunk) {
            dataSet.remove(d);
        }
    }

    /**
     * Set {@code triggerShape} which represents actual clickable
     * location of this data set's gui elements.
     * @param area Shape of gui element (reservoir/pipe)
     */
    public void setTriggerShape(Shape area){
        this.triggerShape = area;
    }

    /**
     * Add measured value passed as parameter into this DataSet with
     * current time from WaterNetwork's simulation time counter.
     * If after adding new record the DataSet's size si too big
     * ({@code desiredMaxDataLength}) then try to reduce it. If
     * reduction was not very successful then increase size limit.
     * @param value recorded value to be stored
     */
    public void putAndTrim(double value){
        this.dataSet.put(this.waterNetwork.currentSimulationTime() * 0.166667, value);
        if (this.dataSet.size() > desiredMaxDataLength){
            reduceDataSet();
            if (this.dataSet.size() > (int)(desiredMaxDataLength * MAX_DATA_LENGTH_RESIZE_EFFORT)){
                desiredMaxDataLength *= MAX_DATA_LENGTH_RESIZE_MULTIPLIER;
            }
        }
    }

    /**
     * Returns true if Point passed as parameter is inside the
     * {@code triggerShape}
     * @param point Point to be tested
     * @return true if point collides with triggerShape
     */
    abstract boolean triggers(Point point);

    /**
     * Collect data without knowing them. (win-win)
     */
    abstract void collectData();

    /**
     * Creates instance of XYGraph showing relation between
     * measured data and time.
     * @return XYGraph built on top of measured data.
     */
    abstract XYGraph getGraph();

    /**
     * Enables auto collection of data. (People are lazy these days)
     * @param delay delay in ms between each data collection
     */
    public void autoCollect(int delay){
        this.timer = new Timer(delay, a -> this.collectData());
        timer.start();
    }

    /**
     * Stops auto collection.
     */
    public void disableAutoCollect(){
        if (this.timer.isRunning()) {
            this.timer.stop();
        }
    }

}

#!/bin/bash

# sync to /drive_c/water-network/ inside prefix and exclude prefix itself
rsync -av ./* ./wine-jdk-prefix/drive_c/water-network/ --exclude=wine-jdk-prefix

# setup wine environment
# java_home is expected to be set
export WINEPREFIX="$(pwd)/wine-jdk-prefix"
export WINEDEBUG=-all
cd ./wine-jdk-prefix/drive_c/water-network

# run winecmd
wine cmd
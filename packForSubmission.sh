mkdir archive
cp -r bin archive/
cp -r src archive/
mkdir archive/doc
cp doc/dokumentace.pdf archive/doc/
cp *.cmd archive/
cp Jama-1.0.3.jar archive/bin/
cd archive
zip -r A17B0333P.zip .
cp A17B0333P.zip ../A17B0333P.zip
cd ../
rm -r archive
